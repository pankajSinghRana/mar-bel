import React from 'react';
import coonLogo from '../images/coonLogo.png';
import '../App.css';

class Header extends React.Component{
    constructor(props){
        super(props);
        this.state = {}
    }

    render(){
        return (
            <div className="header">
                <img className="coon-logo" src={coonLogo} />
            </div>
        );
    }
}


export default Header;