import React from 'react';
import Options from './functionalComponents/Options';
import coon from '../images/character/coon.png';
import mysterion from '../images/character/comicLogo.png';


class Body extends React.Component {
    constructor(props){
        super(props);
        this.state = {choice:[['CHARACTER',coon],['COMICS',mysterion],['MOVIES',mysterion],['WILDCARD',coon]]};
        this.routeChange=this.routeChange.bind(this);
    }

    routeChange() {
        let path = '/char';
        window.location.href = path; 
      }
    
    render() {
        return(
            <div className="MainBody" >
               {this.state.choice.map((obj,i) =>{
                return <Options 
                          imgData={obj[1]} 
                          key={i} 
                          name={obj[0]} 
                          routeChange={this.routeChange}
                    />
               })}
            </div>
        );
    }
}

export default Body;