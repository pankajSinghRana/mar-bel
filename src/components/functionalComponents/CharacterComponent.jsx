import React from 'react';
import coon from '../../images/character/coon.png'

function CharacterComponent (props) {

    console.log(props);

        return(
            <div className='char-component'>
                <div>
                   <img className="char-image" src={coon} />
                </div>
                <div >
                    <p>
                        Name: {props.name}
                    </p>
                    <p>
                        Description: {props.description}
                    </p>
                </div>
            </div>
        );
    
}

export default CharacterComponent;