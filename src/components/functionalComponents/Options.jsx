import React from 'react';


function Options(props) {

  return (
      <div className="choice-div">
        <img 
          src={props.imgData} 
          className="opt-img"
          onClick={props.routeChange}
        />
        <h3><span>{props.name}</span></h3>
      </div>

  );
}

export default Options;