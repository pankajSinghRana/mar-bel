import React from 'react';
import CharacterComponent from '../functionalComponents/CharacterComponent';
import coon from '../../images/character/coon.png';

class MarvelCharacter extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            Imgloading  : true,
            baseCharApi : 'https://gateway.marvel.com:443/v1/public/characters?limit=9&ts=',
            MarvelPublicKey : 'ae92992be7fdde83bc77f676aa486062',
            MarvelPrivateKey : 'bb9bf8b2aa81d045be0556aa7c9b296ccc859d1b',
            results : [{
                name:'coon', 
                description: ' Eric cartman(coon) is a character from south park.The alternate ego CooN is notorious for his daring acts',
                thumbnail : {
                    path : "http://i.annihil.us/u/prod/marvel/i/mg/3/40/4bb4680432f73",
                    extension: "jpg"
                }
            }],
            imageSize : '/portrait_uncanny.'
        };
    }

    componentDidMount(){
        var md5 = require('md5'); 
        var hash = this.calculateHash();

        fetch(this.state.baseCharApi+this.getTimeStamp()+'&apikey='+this.state.MarvelPublicKey+'&hash='+hash)
        .then(data => {return data.json()})
        .then(Response => {
            console.log('inside the response of MarvelCharacter componentDidMount');
            console.log(Response.data.results);
            this.setState({
                results: Response.data.results,
                Imgloading : false
            });
        });
    
    }

    calculateHash = () => {
        var md5 = require('md5'); 
        var tempHash = md5(this.getTimeStamp()+this.state.MarvelPrivateKey+this.state.MarvelPublicKey);
        return tempHash;
    }

    getTimeStamp = () => {
        return Math.floor(Date.now() / 1000);
    }

    getImageLink = (baseUrl, extension) => {
        var imageLink = baseUrl+this.state.imageSize+extension;
        return imageLink;
    }

    render() {
        return(
            <div className ='marvel-main-page'>

                {this.state.results.map((obj,i) =>{
                    return <CharacterComponent 
                            key={i} 
                            imgData={
                                this.state.Imgloading ? (coon):(this.getImageLink(obj.thumbnail.path,obj.thumbnail.extension))
                            }
                            name = {obj.name}
                            description = {obj.description}
                    />
                })}

            </div>
        );
    }
}

export default MarvelCharacter;