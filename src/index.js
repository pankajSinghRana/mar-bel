import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import MarvelCharacter from './components/CharacterPage/MarvelCharacter';
import { BrowserRouter, Route, Link } from "react-router-dom";


let Routes = 
    <BrowserRouter>
        <Route exact  path="/" component={App}/>
        <Route exact path="/char" component={MarvelCharacter}/>
    </BrowserRouter>




ReactDOM.render(Routes, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
